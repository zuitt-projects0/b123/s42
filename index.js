/*
 JS DOM (Document Object Model)

 In CSS, we had the concept of a Box Model. Every element is considered by CSS as a box. In JS, we have Document Object Model. This allows to manipulate our HTML elements with Javascript. Because for Javascript, HTML elements are all considered as objects.

 In such a case, our elements as objects, we can then access and manipulate the properties of an element.

 We can actually do a lot of things with JS DOM, in fact you've seen a sample before wherein when a button is clicked the style of an element changed. For our JS DOM sessions we will focus mainly on the use of Forms.

*/
console.log(document);
//document refers to the whole page.

//.querySelector() is a method that can be used to select a specific element from our document. The querySelector uses CSS like selectors to select an element.

let firstNameLabel = document.querySelector("#label-first-name");

//We were able to select an element by its id from our document and saved it in a variable:
console.log(firstNameLabel);

//.innerHTML is a property of an element which considers all the children of the selected element as string. This includes other elements and text content.
console.log(firstNameLabel.innerHTML);

//We re-assigned the value of the innerHTML property of our firstNameLabel as "I like New York City"
firstNameLabel.innerHTML = "I like New York City.";

/*
	Mini-Activity:

	Select the span with the id label-last-name and save it in a variable called: lastNameLabel

	Then access its innerHTML property and re-assign it to "My favorite food is <favoriteFood>"

*/

let lastNameLabel = document.querySelector("#label-last-name");
lastNameLabel.innerHTML = "My favorite food is Kit Kat. No I'm not sponsored.";

let city = "Tokyo";

//Set a condition wherein if the city variable is not New York, we will show the value otherwise:

if(city === "New York"){

	firstNameLabel.innerHTML = `I like New York City`

} else {

	firstNameLabel.innerHTML = `I don't like New York. I like ${city} city`

}

//Events - allow us to add interactivity to our page. Wherein, we can have our users interact with a page and our page can then perform a task.

/*
	Event Listeners

	Event Listeners allow us to listen or detect an interaction between the user and the page. On the event that the user clicks,presses a key, hover a selected element, we will perform a function.

*/

firstNameLabel.addEventListener('click',()=>{

	firstNameLabel.innerHTML = "I've been clicked, send help!";
	//Elements have a property called style which is the style of an element. style in JS is also an object with properties.
	firstNameLabel.style.color = "red";
	firstNameLabel.style.fontSize = "10vh";

});

/*
	Mini Activity:

	Add an event listener to the lastNameLabel, wherein if we click on the element, the color of the text will change to your favorite color and the size of the text will increase to 5vh.

	Stretch Goal:

	If the color of the text is already your favorite color, when the element is clicked again, change the color back to black, change the font size back to 16px;

*/

lastNameLabel.addEventListener('click',()=>{



	if(lastNameLabel.style.color === "violet"){

		lastNameLabel.style.color = "";
		lastNameLabel.style.fontSize = "";

	} else {

		lastNameLabel.style.color = "violet";
		lastNameLabel.style.fontSize = "5vh";

	}

})

//keyup - is an event wherein we are able to perform a task when the user lets go of a key. Keyup is best used in input elements that require key inputs.


let inputFirstName = document.querySelector("#txt-first-name");
//.value - is a property of mostly input elements which contains the current value in the input element.

//initial value of your input element:
//console.log() ran only the first time our page loaded and does not run again.
console.log(inputFirstName.value);

//select the h3 and save it in a variable:
let fullNameDisplay = document.querySelector("#full-name-display");
let inputLastName = document.querySelector("#txt-last-name");

//syntax: element.addEventListener(<event>,<function>)

//you can create a named function that will be run by event listeners
const showName = () => {
	console.log(inputFirstName.value);
	console.log(inputLastName.value);
	/*
		Mini Activity:

		Add the value of the lastName input in our fullNameDisplay's innerHTML

	*/
	//fullNameDisplay.innerHTML = inputFirstName.value + " " + inputLastName.value

	fullNameDisplay.innerHTML = `${inputFirstName.value} ${inputLastName.value}`;
}

inputFirstName.addEventListener('keyup',showName);


//you can actually create multiple event listeners that run the same function:
inputLastName.addEventListener('keyup',showName);
