let firstNameLabel = document.querySelector("#txt-first-name")
let lastNameLabel = document.querySelector("#txt-last-name")
let fullNameDisplay = document.querySelector("#full-name-display")
let submitz = document.querySelector("#submit-btn")

const showName = () => {
	fullNameDisplay.innerHTML = `${firstNameLabel.value} ${lastNameLabel.value}`
}

const alertValidation = () => {
	if(firstNameLabel.value && lastNameLabel.value){
		alert(`Thank you for registering ${firstNameLabel.value} ${lastNameLabel.value}`)
		firstNameLabel.value = ""
		lastNameLabel.value = ""
		fullNameDisplay.innerHTML = ""
	}
	else{
		alert("Please input firsname and lastname.")
	}
}


firstNameLabel.addEventListener('keyup',showName)
lastNameLabel.addEventListener('keyup',showName)

submitz.addEventListener('click',alertValidation)


